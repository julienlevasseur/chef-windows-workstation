name 'windows-workstation'
maintainer 'Julien Levasseur'
maintainer_email 'julien@julienlevasseur.ca'
license 'All Rights Reserved'
description 'Installs/Configures windows-workstation'
long_description 'Installs/Configures windows-workstation'
version '0.1.0'
chef_version '>= 12.14' if respond_to?(:chef_version)

issues_url 'https://gitlab.com/julienlevasseur/windows-workstation/issues'
source_url 'https://gitlab.com/julienlevasseur/windows-workstation'

depends 'chocolatey'
