#
# Cookbook:: windows-workstation
# Recipe:: chocolatey
#
# Copyright:: 2018, Julien Levasseur, All Rights Reserved.

node['windows-workstation']['chocolatey']['packages'].each do |pkg|
  chocolatey_package pkg do
    action :install
  end
end
